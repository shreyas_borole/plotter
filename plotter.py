import pygame
import pygame_gui
import numpy as np

pygame.init()

font = pygame.font.SysFont('notosansmono',24)
font_label = pygame.font.SysFont('liberationserif',12)

black = (0,0,0)
white = (255,255,255)
bg_color = (40,44,53)
grid_color = (135,206,235)	#or (157,205,101)
title_color = (250,0,0)
label_color = (120,120,120)

width, height = 700, 700
extraW = 600
screen = pygame.display.set_mode((width+extraW,height))	# pygame.RESIZABLE)
manager = pygame_gui.UIManager((width+extraW,height))
k = []
k.append(25)

eq = []
pygame.display.set_caption("Plotter")
screen.fill(bg_color)

screen.set_clip(width,0,width+extraW,height)
background = pygame.Surface((extraW, height))
background.fill(bg_color)
screen.set_clip(None)

def grid(k,scale=1):
    #clipping
    screen.set_clip(0,0,width,height)
    screen.fill(white)
    for i in range(int(width/k[0]*scale)):
        gridx = gridy = k[0]*i*scale
        pygame.draw.line(screen, grid_color, (gridx,0), (gridx,height), 1)
        pygame.draw.line(screen, grid_color, (0,gridy), (width,gridy), 1)
    pygame.draw.line(screen, grid_color, (width, 0), (width, height), 5)
    #axes
    midx, midy = int(width/(k[0]*2)), int(height/(k[0]*2))
    pygame.draw.line(screen, (33, 47, 44), (midx*k[0], 0), (midx*k[0], height), 2)
    pygame.draw.line(screen, (33, 47, 44), (0,midy*k[0]), (width, midy*k[0]), 2)
    #For +ve xy-labels
    for i in range(1, int(width/(k[0]*2))):
        if k[0] == 10:
            if i%2 == 0:
            	label = font_label.render(str(i), 1, label_color)
            	x_conv = k[0]*i + int(width/2)
            	y_conv = int(height/2)
            	screen.blit(label, (x_conv - 2, y_conv + 10))
            	x_conv = int(width/2)
            	y_conv -= k[0]*i
            	screen.blit(label, (x_conv + 10, y_conv - 2))
            else:
                pass
        else:
            label = font_label.render(str(i), 1, label_color)
            x_conv = k[0]*i + int(width/2)
            y_conv = int(height/2)
            screen.blit(label, (x_conv - 2, y_conv + 10))
            x_conv = int(width/2)
            y_conv -= k[0]*i
            screen.blit(label, (x_conv + 10, y_conv - 2))
    #For -ve xy-labels
    for i in range(-int(width/(k[0]*2)), 0):
        if k[0] == 10:
            if i%2 == 0:
            	label = font_label.render(str(i), 1, label_color)
            	x_conv = k[0]*i + int(width/2)
            	y_conv = k[0]*0 + int(height/2)
            	screen.blit(label, (x_conv - 2, y_conv + 10))
            	x_conv = int(width/2)
            	y_conv -= k[0]*i
            	screen.blit(label, (x_conv + 10, y_conv - 2))
        else:
            label = font_label.render(str(i), 1, label_color)
            x_conv = k[0]*i + int(width/2)
            y_conv = k[0]*0 + int(height/2)
            screen.blit(label, (x_conv - 2, y_conv + 10))
            x_conv = int(width/2)
            y_conv -= k[0]*i
            screen.blit(label, (x_conv + 10, y_conv - 2))
        #For origin
        label = font_label.render("(0,0)", 1, label_color)
        screen.blit(label, (int(width/2) - 2, int(height/2) + 10))
    #reset clipping
    screen.set_clip(None)

#Zoom UI
pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((width+10, 20), (40, 20)),
    text="Zoom:", manager=manager)
drop_down = pygame_gui.elements.ui_drop_down_menu.UIDropDownMenu(
	['Zoom 1', 'Zoom 2', 'Zoom 3'], 'Zoom 2', relative_rect=pygame.Rect((width+60, 20), (200, 20)),
    manager=manager)
#ListBox UI
list_points = []
str_list = []
list_html = pygame_gui.elements.ui_drop_down_menu.UIDropDownMenu(
	str_list, 'Point(s)', relative_rect=pygame.Rect((width+180, 75), (100, 20)),
    manager=manager)
pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((width+10, 75), (50, 20)),
    text="Point:", manager=manager)
text_box_add = pygame_gui.elements.UITextEntryLine(relative_rect=pygame.Rect((width+70, 70),(100, 20)),manager=manager)
#Plot btn
plot_btn = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((width+10, height - 40), (100, 20)),
    text='Plot',manager=manager)
#Transformation UI
lists = []
lists_str = []
count = 1
pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((width+10, 120), (150, 20)),
    text="Transformation:", manager=manager)
list_html_trans = pygame_gui.elements.ui_drop_down_menu.UIDropDownMenu(
	lists_str, 'Transformation(s)', relative_rect=pygame.Rect((width+20, 150), (200, 20)),
    manager=manager)
#Row 1
l1 = pygame_gui.elements.UILabel(relative_rect=pygame.Rect((width+20, 200), (50, 20)),text='0',manager=manager)
l2 = pygame_gui.elements.UILabel(relative_rect=pygame.Rect((width+100, 200), (50, 20)),text='0',manager=manager)
l3 = pygame_gui.elements.UILabel(relative_rect=pygame.Rect((width+180, 200), (50, 20)),text='0',manager=manager)
#Row 2
l4 = pygame_gui.elements.UILabel(relative_rect=pygame.Rect((width+20, 250), (50, 20)),text='0',manager=manager)
l5 = pygame_gui.elements.UILabel(relative_rect=pygame.Rect((width+100, 250), (50, 20)),text='0',manager=manager)
l6 = pygame_gui.elements.UILabel(relative_rect=pygame.Rect((width+180, 250), (50, 20)),text='0',manager=manager)
#Row 2
l7 = pygame_gui.elements.UILabel(relative_rect=pygame.Rect((width+20, 300), (50, 20)),text='0',manager=manager)
l8 = pygame_gui.elements.UILabel(relative_rect=pygame.Rect((width+100, 300), (50, 20)),text='0',manager=manager)
l9 = pygame_gui.elements.UILabel(relative_rect=pygame.Rect((width+180, 300), (50, 20)),text='0',manager=manager)

text_box_add_trans = pygame_gui.elements.UITextEntryLine(relative_rect=pygame.Rect((width+20, 350),(200, 20)),manager=manager)
#Transform btn
trans_btn = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((width+130, height - 40), (100, 20)),
    text='Transform',manager=manager)
#Identity Matrix btn
iden_btn = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((width+230, 150), (200, 20)),
    text='Load Identity Matrix',manager=manager)
#Sliders
pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((width+10, 400), (150, 20)),
    text="Scaling in X", manager=manager)
s1 = pygame_gui.elements.UIHorizontalSlider(relative_rect=pygame.Rect((width+10, 430), (250, 20)),
                                       start_value=0,
                                       value_range=(0, 100),
                                       manager=manager)
pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((width+10, 460), (150, 20)),
    text="Scaling in Y", manager=manager)
s2 = pygame_gui.elements.UIHorizontalSlider(relative_rect=pygame.Rect((width+10, 490), (250, 20)),
                                       start_value=0,
                                       value_range=(0, 100),
                                       manager=manager)
pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((width+10, 520), (150, 20)),
    text="Shearing in X", manager=manager)
s3 = pygame_gui.elements.UIHorizontalSlider(relative_rect=pygame.Rect((width+10, 550), (250, 20)),
                                       start_value=0,
                                       value_range=(0, 100),
                                       manager=manager)
pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((width+10, 580), (150, 20)),
    text="Shearing in Y", manager=manager)
s4 = pygame_gui.elements.UIHorizontalSlider(relative_rect=pygame.Rect((width+10, 610), (250, 20)),
                                       start_value=0,
                                       value_range=(0, 100),
                                       manager=manager)
pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((width+310, 260), (150, 20)),
    text="Translation in X-", manager=manager)
s9 = pygame_gui.elements.UIHorizontalSlider(relative_rect=pygame.Rect((width+310, 290), (250, 20)),
                                       start_value=0,
                                       value_range=(0, 100),
                                       manager=manager)
pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((width+310, 330), (150, 20)),
    text="Translation in Y-", manager=manager)
s10 = pygame_gui.elements.UIHorizontalSlider(relative_rect=pygame.Rect((width+310, 360), (250, 20)),
                                       start_value=0,
                                       value_range=(0, 100),
                                       manager=manager)
# pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((width+310, 190), (150, 20)),
#     text="Rotation", manager=manager)
# s11 = pygame_gui.elements.UIHorizontalSlider(relative_rect=pygame.Rect((width+310, 230), (250, 20)),
#                                        start_value=0,
#                                        value_range=(0, 100),
#                                        manager=manager)
pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((width+310, 400), (150, 20)),
    text="Translation in X+", manager=manager)
s5 = pygame_gui.elements.UIHorizontalSlider(relative_rect=pygame.Rect((width+310, 430), (250, 20)),
                                       start_value=0,
                                       value_range=(0, 100),
                                       manager=manager)
pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((width+310, 460), (150, 20)),
    text="Translation in Y+", manager=manager)
s6 = pygame_gui.elements.UIHorizontalSlider(relative_rect=pygame.Rect((width+310, 490), (250, 20)),
                                       start_value=0,
                                       value_range=(0, 100),
                                       manager=manager)
pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((width+310, 520), (150, 20)),
    text="Reflection in X", manager=manager)
s7 = pygame_gui.elements.UIHorizontalSlider(relative_rect=pygame.Rect((width+310, 550), (250, 20)),
                                       start_value=0,
                                       value_range=(0, 100),
                                       manager=manager)
pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((width+310, 580), (150, 20)),
    text="Reflection in Y", manager=manager)
s8 = pygame_gui.elements.UIHorizontalSlider(relative_rect=pygame.Rect((width+310, 610), (250, 20)),
                                       start_value=0,
                                       value_range=(0, 100),
                                       manager=manager)
#Clear btn
clear_btn = pygame_gui.elements.UIButton(relative_rect=pygame.Rect((width+250, height - 40), (100, 20)),
    text='Clear',manager=manager)

#Plot Function
pygame_gui.elements.ui_label.UILabel(relative_rect=pygame.Rect((width+310, 45), (50, 20)),
    text="y =", manager=manager)
text_box_func = pygame_gui.elements.UITextEntryLine(relative_rect=pygame.Rect((width+360, 40),(100, 20)),manager=manager)

def plot_point(x, y, k, color=black):
    screen.set_clip(0,0,width,height)

    x_conv = k[0]*x + int(width/2)
    y_conv = k[0]*-y + int(height/2)
    x_conv = round(x_conv)
    y_conv = round(y_conv)
    pygame.draw.circle(screen, color, (x_conv,y_conv),3,0)
    if color != black:
        temp = (122,92,88)
    else:
        temp = title_color
    label = font_label.render("("+str(x)+","+str(y)+")", 1, temp)
    screen.blit(label, (x_conv + 10, y_conv - 10))

    screen.set_clip(None)
grid(k)

def convert_list_coordinates(list, k):
    temp_list = []
    for i in list:
        x_conv = k[0]*i[0] + int(width/2)
        y_conv = k[0]*-i[1] + int(height/2)
        x_conv = round(x_conv)
        y_conv = round(y_conv)
        temp_list.append((x_conv, y_conv))
    return temp_list

def plot(list, k, color = (157,205,101)):
    if len(list_points) != 0:
        for point in list_points:
            plot_point(point[0], point[1], k)
        screen.set_clip(0,0,width,height)
        temp_list = convert_list_coordinates(list, k)
        if len(list_points) != 1:
            line = pygame.draw.lines(screen, color, True, temp_list, 3)
        screen.set_clip(None)

def multiply(list_points, lists, k, clear = False):
    if clear == True:
        grid(k)
    t_ans = ([1,0,0],[0,1,0],[0,0,1])
    global count
    for i in range(count - 1):
        t_ans = np.dot(t_ans, (lists[i][0], lists[i][1], lists[i][2]))
    ans = np.dot((list_points),t_ans)
    print("Transformed Result = ", ans)
    screen.set_clip(0,0,width,height)

    plot(ans, k, (100,245,141))
    for i in range(len(ans)):
        plot_point(round(float(ans[i][0])), round(float(ans[i][1])), k, (122,92,88))

    screen.set_clip(None)

def update_graph(obj, i, j, mul = 1):
    if obj.get_current_value() > 0 and obj.get_current_value() <= 10:
        lists[0][i][j] += 0*mul
        print(lists)
        multiply(list_points, lists, k, True)
        plot(list_points, k)
        lists[0][i][j] -= 0*mul
    if obj.get_current_value() > 10 and obj.get_current_value() <= 20:
        lists[0][i][j] += 1*mul
        print(lists)
        multiply(list_points, lists, k, True)
        plot(list_points, k)
        lists[0][i][j] -= 1*mul
    if obj.get_current_value() > 20 and obj.get_current_value() <= 30:
        lists[0][i][j] += 2*mul
        print(lists)
        multiply(list_points, lists, k, True)
        plot(list_points, k)
        lists[0][i][j] -= 2*mul
    if obj.get_current_value() > 30 and obj.get_current_value() <= 40:
        lists[0][i][j] += 3*mul
        print(lists)
        multiply(list_points, lists, k, True)
        plot(list_points, k)
        lists[0][i][j] -= 3*mul
    if obj.get_current_value() > 40 and obj.get_current_value() <= 50:
        lists[0][i][j] += 4*mul
        print(lists)
        multiply(list_points, lists, k, True)
        plot(list_points, k)
        lists[0][i][j] -= 4*mul
    if obj.get_current_value() > 50 and obj.get_current_value() <= 60:
        lists[0][i][j] += 5*mul
        print(lists)
        multiply(list_points, lists, k, True)
        plot(list_points, k)
        lists[0][i][j] -= 5*mul
    if obj.get_current_value() > 60 and obj.get_current_value() <= 70:
        lists[0][i][j] += 6*mul
        print(lists)
        multiply(list_points, lists, k, True)
        plot(list_points, k)
        lists[0][i][j] -= 6*mul
    if obj.get_current_value() > 70 and obj.get_current_value() <= 80:
        lists[0][i][j] += 7*mul
        print(lists)
        multiply(list_points, lists, k, True)
        plot(list_points, k)
        lists[0][i][j] -= 7*mul
    if obj.get_current_value() > 80 and obj.get_current_value() <= 90:
        lists[0][i][j] += 8*mul
        print(lists)
        multiply(list_points, lists, k, True)
        plot(list_points, k)
        lists[0][i][j] -= 8*mul
    if obj.get_current_value() > 90 and obj.get_current_value() <= 100:
        lists[0][i][j] += 9*mul
        print(lists)
        multiply(list_points, lists, k, True)
        plot(list_points, k)
        lists[0][i][j] -= 9*mul

# temp_rot = [[np.cos(0), np.sin(0), 0], [-np.sin(0),np.cos(0), 0], [0,0,1]]

# def update_graph_rotation(obj):
#     for z in range(1, 12):
#         if obj.get_current_value() > 8*(z-1) and obj.get_current_value() <= 8*z:
#             angle = (np.pi/180)*30*(z-1)
#             temp_rot[0][0] += np.cos(angle)
#             temp_rot[0][1] += np.sin(angle)
#             temp_rot[1][0] += -np.sin(angle)
#             temp_rot[1][1] += np.cos(angle)
#             print(temp_rot)
#             multiply(list_points, temp_rot, k, True)
#             plot(list_points, k)
#             temp_rot[0][0] -= np.cos(angle)
#             temp_rot[0][1] -= np.sin(angle)
#             temp_rot[1][0] -= -np.sin(angle)
#             temp_rot[1][1] -= np.cos(angle)

def plot_function(eq, k):
    if len(eq) != 0:
        for i in range(width):
            x = (width/2-i)/float(k[0])
            y = eval(eq[0])
            pos1 = (width/2+x*k[0], height/2-y*k[0])
            nx=x=(width/2-(i+1))/float(k[0])
            ny=eval(eq[0])
            pos2 = (width/2+nx*k[0], height/2-ny*k[0])
            pygame.draw.aaline(screen, (255,0,0), pos1, pos2, 3)
pygame.mouse.set_pos(int(width/2), int(height/2))
clock = pygame.time.Clock()


def process_events():
    if(event.type == pygame.USEREVENT and event.user_type == pygame_gui.UI_DROP_DOWN_MENU_CHANGED and
            event.ui_element == drop_down):
        if event.text == 'Zoom 3':
            k[0] = 10
            grid(k)
            plot_function(eq, k)
            if len(list_points) != 0:
                plot(list_points, k)
                multiply(list_points, lists, k)
        elif event.text == 'Zoom 2':
            k[0] = 25
            grid(k)
            plot_function(eq, k)
            if len(list_points) != 0:
                plot(list_points, k)
                multiply(list_points, lists, k)
        elif event.text == 'Zoom 1':
            k[0] = 35
            grid(k)
            plot_function(eq, k)
            if len(list_points) != 0:
                plot(list_points, k)
                multiply(list_points, lists, k)
    if event.type == pygame.USEREVENT:
        if event.user_type == pygame_gui.UI_TEXT_ENTRY_FINISHED and event.ui_element == text_box_add:
            str = event.text
            str.strip()
            str_list.append(str)
            temp = []
            temp = str.split(",")
            temp = [float(i) for i in temp]
            temp.append(1)
            list_points.append(temp)
            print("Point added: ", temp)
            text_box_add.set_text("")
        if event.user_type == pygame_gui.UI_TEXT_ENTRY_FINISHED and event.ui_element == text_box_add_trans:
            list_trans = [[],[],[]]
            str_list_trans = [['','',''],['','',''],['','','']]
            str = event.text
            str.strip()
            temp = []
            temp = str.split(",")
            temp[2] = temp[2].split(";")
            temp[4] = temp[4].split(";")
            temp2 = []
            temp2.append(temp[0])
            temp2.append(temp[1])
            temp2.append(temp[2][0])
            temp2.append(temp[2][1])
            temp2.append(temp[3])
            temp2.append(temp[4][0])
            temp2.append(temp[4][1])
            temp2.append(temp[5])
            temp2.append(temp[6])
            del temp
            z=0
            for j in range(3):
                for i in range(3):
                    str_list_trans[j][i] = temp2[z][:]
                    z+=1
            temp2 = [float(i) for i in temp2]
            z=0
            for j in range(3):
                for i in range(3):
                    list_trans[j].append(temp2[z])
                    z+=1
            l1.set_text(str_list_trans[0][0])
            l2.set_text(str_list_trans[0][1])
            l3.set_text(str_list_trans[0][2])
            l4.s7et_text(str_list_trans[1][0])
            l5.set_text(str_list_trans[1][1])
            l6.set_text(str_list_trans[1][2])
            l7.set_text(str_list_trans[2][0])
            l8.set_text(str_list_trans[2][1])
            l9.set_text(str_list_trans[2][2])
            lists.append(list_trans)
            print(lists)
            global count
            lists_str.append('%d' %(count))
            count += 1
            text_box_add_trans.set_text("")
        if event.user_type == pygame_gui.UI_TEXT_ENTRY_FINISHED and event.ui_element == text_box_func:
            eq.append('')
            eq[0] = event.text
            plot_function(eq, k)
            text_box_func.set_text("")
    update_graph(s1,0,0)
    update_graph(s2,1,1)
    update_graph(s3,1,0)
    update_graph(s4,0,1)
    update_graph(s5,2,0)
    update_graph(s6,2,1)
    update_graph(s7,0,0,-1)
    update_graph(s8,1,1,-1)
    update_graph(s9,2,0,-1)
    update_graph(s10,2,1,-1)
    # update_graph_rotation(s11)
    if(event.type == pygame.USEREVENT and event.user_type == 'ui_button_pressed' and
            event.ui_element == plot_btn):
            plot(list_points, k)
    if(event.type == pygame.USEREVENT and event.user_type == 'ui_button_pressed' and
            event.ui_element == clear_btn):
            grid(k)
    if(event.type == pygame.USEREVENT and event.user_type == 'ui_button_pressed' and
            event.ui_element == iden_btn):
            list_trans = [[],[],[]]
            str_list_trans = [['','',''],['','',''],['','','']]
            str = "1,0,0;0,1,0;0,0,1"
            temp = []
            temp = str.split(",")
            temp[2] = temp[2].split(";")
            temp[4] = temp[4].split(";")
            temp2 = []
            temp2.append(temp[0])
            temp2.append(temp[1])
            temp2.append(temp[2][0])
            temp2.append(temp[2][1])
            temp2.append(temp[3])
            temp2.append(temp[4][0])
            temp2.append(temp[4][1])
            temp2.append(temp[5])
            temp2.append(temp[6])
            del temp
            z=0
            for j in range(3):
                for i in range(3):
                    str_list_trans[j][i] = temp2[z][:]
                    z+=1
            temp2 = [float(i) for i in temp2]
            z=0
            for j in range(3):
                for i in range(3):
                    list_trans[j].append(temp2[z])
                    z+=1
            l1.set_text(str_list_trans[0][0])
            l2.set_text(str_list_trans[0][1])
            l3.set_text(str_list_trans[0][2])
            l4.set_text(str_list_trans[1][0])
            l5.set_text(str_list_trans[1][1])
            l6.set_text(str_list_trans[1][2])
            l7.set_text(str_list_trans[2][0])
            l8.set_text(str_list_trans[2][1])
            l9.set_text(str_list_trans[2][2])
            lists.append(list_trans)
            print(lists)
            # global count
            lists_str.append('%d' %(count))
            count += 1
            text_box_add_trans.set_text("")
    if(event.type == pygame.USEREVENT and event.user_type == 'ui_button_pressed' and
            event.ui_element == trans_btn):
        if len(list_points) != 0:
            multiply(list_points, lists, k)
    if(event.type == pygame.USEREVENT and event.user_type == pygame_gui.UI_DROP_DOWN_MENU_CHANGED and
            event.ui_element == list_html):
            temp = []
            temp = event.text.split(',')
            temp = [float(i) for i in temp]
            temp.append(1)
            print(temp)
            list_points.remove(temp)
            str_list.remove(event.text)
            grid(k)
            print("Point deleted: ", temp)
    if(event.type == pygame.USEREVENT and event.user_type == pygame_gui.UI_DROP_DOWN_MENU_CHANGED and
            event.ui_element == list_html_trans):
            l1.set_text("%.1f" %(lists[int(event.text) - 1][0][0]))
            l2.set_text("%.1f" %(lists[int(event.text) - 1][0][1]))
            l3.set_text("%.1f" %(lists[int(event.text) - 1][0][2]))
            l4.set_text("%.1f" %(lists[int(event.text) - 1][1][0]))
            l5.set_text("%.1f" %(lists[int(event.text) - 1][1][1]))
            l6.set_text("%.1f" %(lists[int(event.text) - 1][1][2]))
            l7.set_text("%.1f" %(lists[int(event.text) - 1][2][0]))
            l8.set_text("%.1f" %(lists[int(event.text) - 1][2][1]))
            l9.set_text("%.1f" %(lists[int(event.text) - 1][2][2]))

is_running = True
while is_running:
    time_delta = clock.tick(60)/1000.0
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            is_running = False
        process_events()

        manager.process_events(event)
    manager.update(time_delta)

    screen.blit(background, (width, 0))

    manager.draw_ui(screen)
    pygame.display.update()
